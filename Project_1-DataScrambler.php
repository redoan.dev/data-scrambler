<?php
session_start();
include_once "Project_1f.php";
$task="";
if(isset($_GET["task"]) && $_GET["task"] !=''){
    $task=$_GET["task"];}
if ("decode"==$task || "encode"==$task || "key"==$task) {
    $_SESSION["key"];
    }else{session_destroy(); 
    }
$result="";
$key="abcdefghijklmnopqrstuvwxyz0123456789";
if("key"==$task){
    $key=str_shuffle($key);
    $_POST["key"]=$key;
    //$keyGenerate=str_split($key);
    //shuffle($keyGenerate);
    //$key=join("",$keyGenerate);  
}elseif(isset($_POST["key"]) && $_POST["key"] !=''){
    $key=$_POST["key"];
    }
if(isset($_POST['key']) && $_POST['key']!=''){
    $_SESSION['key'] = $_POST['key'];
    }else {$_SESSION['key'];}
if(isset($_POST["data"]) && $_POST["data"] !=''){
    $data=$_POST["data"];
    if("decode"==$task){
        $result= decodeData($data,$key);
    }
    elseif("encode"==$task){
        $result= encodeData($data,$key);
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>DATA SCRAMBLER</title>
        <link rel="stylesheet" href="assests/css.css">
        <link rel="stylesheet" href="assests/normalize.css">
        <link rel="stylesheet" href="assests/milligram.min.css"> 
        <style>
            body{
                margin-top: 30px;
            }
            #data{
                width: 100%;
                height: 200px;
            }
            #result{
                width: 100%;
                height: 200px;
            }
            .hidden{
                display: none;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="column column-60 column-offset-20">
                    <h1>DATA SCRAMBLER</h1>
                    <p>Use this application to scramble your data</p>
                    <p>
                        <a href="http://localhost/learnPHP/DataScrambler/Project_1-DataScrambler.php?task=encode">Encode</a> |
                        <a href="http://localhost/learnPHP/DataScrambler/Project_1-DataScrambler.php?task=decode">Decode</a> |
                        <a href="http://localhost/learnPHP/DataScrambler/Project_1-DataScrambler.php?task=key">Generate Key</a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="column column-60 column-offset-20">
                    <form method="POST" action="Project_1-DataScrambler.php <?php switch ($task) {case "decode":{echo "?task=decode"; break;} default: echo "?task=encode";} ?>">
                        <label for="key">KEY</label>
                        <input type="text" name="key" id="key" value='<?php echo keyOrSession($key) ?>'>
                        <label for="data">DATA</label>
                        <textarea type="text" name="data" id="data"><?php echo $data;?></textarea>
                        <label for="result">RESULT</label>
                        <textarea type="text" name="result" id="result"><?php echo $result;?></textarea>
                        <button type="submit">DO IT FOR ME</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
